﻿using log4net;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Configuration;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

namespace botDrive.Proceso
{
    class SubidaExcel
    {
        private static ILog log = LogManager.GetLogger(typeof(SubidaExcel));

        private static string rutaHomologacion = ConfigurationManager.AppSettings["rutaHomologacionArchivosExcelCsv"];
        private static string rutaSubido = ConfigurationManager.AppSettings["rutaSubidaArchivosExcelCsv"];
        private static string rutaNOSubido = ConfigurationManager.AppSettings["rutaNOSubidaArchivosExcelCsv"];

        private string urlPage = ConfigurationManager.AppSettings["iAgree_url"]; 
        private string urlPageLogged = ConfigurationManager.AppSettings["iAgreeLogeado_url"];

        protected By Options = By.ClassName("profile-name");
        protected By Inicio = By.Id("mainForm:j_idt56");
        protected By Inicio2 = By.Id("mainForm:j_idt55");
        protected By Buscador = By.Id("mainForm:dtGrupoCampanas:nomGrupo");
        protected By Entidad = By.Id("mainForm:dtGrupoCampanas:nomGrupo");
        protected By Detalle = By.Id("mainForm:dtCampanas:0:j_idt198");
        protected By NombreDetalle = By.Id("mainForm:dtCampanas:j_idt193");        
        protected By Importar = By.Id("mainForm:mnImportar");
        protected By SelectTipo = By.Id("mainForm:j_idt142_label");
        protected By TipoMasivoArbol = By.Id("mainForm:j_idt142_9"); 
        protected By SelectEstructura = By.Id("mainForm:somConfigCagues_label");
        protected By EstructuraSMS = By.Id("mainForm:somConfigCagues_2");
        protected By SubirArchivo = By.Id("mainForm:fileUpload_input");
        protected By EnviarArchivo = By.ClassName("ui-button-text");    

        protected By prefixCaptcha = By.Id("loginb:txtCaptchaId");
                
        protected IWebDriver Driver;


        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void keybd_event(uint bVk, uint bScan, uint dwFlags, uint dwExtraInfo);

        [SetUp]
        public void beforeText()
        {
            log.Info("Se inicia el navegador. Fecha: " + DateTime.Now.ToString());
            ChromeOptions options = new ChromeOptions();
            Driver = new ChromeDriver(options);

            WebDriverWait ewait = new WebDriverWait(Driver, new TimeSpan(0, 5, 0));

            string[] files = Directory.GetFiles(rutaHomologacion, "*.csv", SearchOption.AllDirectories);

            log.Info("Se inicia la busqueda de archivos homologados. Fecha: " + DateTime.Now.ToString());

            if (files != null && files.Length > 0)
            {

                log.Info("Se encontraron los archivos homologados correctamente. Fecha: " + DateTime.Now.ToString());

                Driver.Navigate().GoToUrl(urlPage);
                Driver.Manage().Window.Maximize();

                bool flagLoggedIn = false;
                do
                {
                    log.Info("A la espera de que el usuario inicie sesión, tiempo de espera : 5 minutos... . Fecha: " + DateTime.Now.ToString());
                    try
                    {
                        ewait.Until(ExpectedConditions.UrlMatches(urlPageLogged));
                        Thread.Sleep(1500);
                        if (Driver.Url == urlPageLogged)
                        {
                            flagLoggedIn = true;
                            log.Info("Se detectó el inicio de sesión. Fecha: " + DateTime.Now.ToString());
                        }
                    }
                    catch(Exception e)
                    {
                        log.Error("NO SE DETECTÓ el inicio de sesión dentro del tiempo de espera : 5 minutos... . Fecha: " + DateTime.Now.ToString());
                    }
                }
                while (flagLoggedIn == false);
                Thread.Sleep(1500);

                ewait.Until(ExpectedConditions.ElementExists(Options));
                ewait.Until(ExpectedConditions.ElementToBeClickable(Options));
                Driver.FindElement(Options).Click();

                log.Info("Se inicia la búsqueda de campañas para la subida de archivos homologados. Fecha: " + DateTime.Now.ToString());
                foreach (var file in files)
                {
                    try
                    {
                        string gestionesEntidad = Path.GetFileName(file).Remove(Path.GetFileName(file).Length - 4, 4);;
                        string nombreGestion = Helpers.ConvertGestionEntidad.obtenerGestiones(gestionesEntidad);

                        log.Info("Se inicia la búsqueda de campaña: " + nombreGestion + ". Fecha: " + DateTime.Now.ToString());

                        ewait.Until(ExpectedConditions.ElementExists(Inicio));
                        ewait.Until(ExpectedConditions.ElementToBeClickable(Inicio));
                        Driver.FindElement(Inicio).Click();
                        ewait.Until(ExpectedConditions.ElementExists(Buscador));
                        ewait.Until(ExpectedConditions.ElementToBeClickable(Buscador));
                        Thread.Sleep(3000);
                        Driver.FindElement(Buscador).Click();
                        Driver.FindElement(Buscador).SendKeys(nombreGestion);
                        Thread.Sleep(20000);
                        ewait.Until(ExpectedConditions.ElementExists(By.XPath("//div[@class='ui-datatable-scrollable-body']/table")));
                        ewait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//div[@class='ui-datatable-scrollable-body']/table")));
                        IWebElement table = Driver.FindElement(By.XPath("//div[@class='ui-datatable-scrollable-body']/table"));
                        IWebElement customer = table.FindElement(By.XPath("//tr/td[contains(text(), '" + nombreGestion.ToUpper() + "')]"));
                        customer.Click();

                        if (nombreGestion == "50" || nombreGestion == "41")
                        {
                            ewait.Until(ExpectedConditions.ElementExists(NombreDetalle));
                            ewait.Until(ExpectedConditions.ElementToBeClickable(NombreDetalle));
                            Driver.FindElement(NombreDetalle).Click();
                            if (nombreGestion == "50")
                            {
                                Driver.FindElement(NombreDetalle).SendKeys(Constantes.ConstantesEntidad.SECREDITOS_CASTIGO);
                            }
                            else if (nombreGestion == "41")
                            {
                                Driver.FindElement(NombreDetalle).SendKeys(Constantes.ConstantesEntidad.SECREDITOS_PRE);
                            }
                            Thread.Sleep(15000);
                        }
                        ewait.Until(ExpectedConditions.ElementExists(Detalle));
                        ewait.Until(ExpectedConditions.ElementToBeClickable(Detalle));
                        Driver.FindElement(Detalle).Click();

                        log.Info("Se ingresa al detalle de campaña: " + nombreGestion + ". Fecha: " + DateTime.Now.ToString());

                        Thread.Sleep(5000);
                        for (int c = 0; c < 10; c++)
                        {
                            Thread.Sleep(100);
                            keybd_event(0x09, 0, 0, 0);
                        }
                        for (int c = 0; c < 10; c++)
                        {
                            Thread.Sleep(100);
                            keybd_event(0x28, 0, 0, 0);
                        }
                        Driver.Manage().Window.FullScreen();
                        Thread.Sleep(3000);
                        //Driver.FindElement(Exportar).FindElement(By.XPath("//a/span[contains(text(), 'Exportar')]")).FindElement(By.XPath("//a/span[contains(text(), 'Informes')]")).Click();
                        
                        ewait.Until(ExpectedConditions.ElementExists(Importar));
                        ewait.Until(ExpectedConditions.ElementToBeClickable(Importar));
                        Driver.FindElement(Importar).FindElement(By.XPath("//a/span[contains(text(), 'Importar')]")).Click();


                        log.Info("Se inicia la importacion de archivo para la campaña: " + nombreGestion + ". Fecha: " + DateTime.Now.ToString());
                        Thread.Sleep(3000);
                        Driver.Manage().Window.Maximize();
                        Thread.Sleep(3000);
                        ewait.Until(ExpectedConditions.ElementExists(SelectTipo));
                        ewait.Until(ExpectedConditions.ElementToBeClickable(SelectTipo));
                        Driver.FindElement(SelectTipo).Click();
                        ewait.Until(ExpectedConditions.ElementExists(TipoMasivoArbol));
                        ewait.Until(ExpectedConditions.ElementToBeClickable(TipoMasivoArbol));
                        Driver.FindElement(TipoMasivoArbol).Click();
                        Thread.Sleep(2000);
                        ewait.Until(ExpectedConditions.ElementExists(SelectEstructura));
                        ewait.Until(ExpectedConditions.ElementToBeClickable(SelectEstructura));
                        Driver.FindElement(SelectEstructura).Click();
                        ewait.Until(ExpectedConditions.ElementExists(EstructuraSMS));
                        ewait.Until(ExpectedConditions.ElementToBeClickable(EstructuraSMS));
                        Driver.FindElement(EstructuraSMS).Click();
                        ewait.Until(ExpectedConditions.ElementExists(SubirArchivo));
                        Driver.FindElement(SubirArchivo).SendKeys(file);

                        log.Info("Se envia el archivo de la campaña: " + nombreGestion + ". Fecha: " + DateTime.Now.ToString());
                        Thread.Sleep(2000);
                        ewait.Until(ExpectedConditions.ElementExists(By.XPath("//button/span[contains(text(), 'Cargar')]")));
                        ewait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//button/span[contains(text(), 'Cargar')]")));
                        Driver.FindElement(EnviarArchivo).FindElement(By.XPath("//button/span[contains(text(), 'Cargar')]")).Click();

                        log.Info("Se inicia la carga del archivo para la campaña: " + nombreGestion + ". Fecha: " + DateTime.Now.ToString());
                        bool flagUsoArchivo = true;
                        do
                        {
                            Thread.Sleep(30000);
                            try
                            {
                                File.Move(file, rutaSubido + "\\" + Path.GetFileName(file));

                                log.Info("Se movio el archivo "+file+" de la campaña: " + nombreGestion + " a la carpeta de subidos. Fecha: " + DateTime.Now.ToString());
                                flagUsoArchivo = false;
                            }
                            catch(Exception e)
                            {

                            }
                        } while (flagUsoArchivo == true);

                        ewait.Until(ExpectedConditions.ElementExists(Inicio2));
                        ewait.Until(ExpectedConditions.ElementToBeClickable(Inicio2));
                        Driver.FindElement(Inicio2).Click();
                        log.Info("Se volvió al menu principal para realizar la siguiente subida. Fecha: " + DateTime.Now.ToString());

                    }
                    catch (Exception e)
                    {
                        log.Error("No se pudo subir el archivo csv. La entidad no existe o se arrojo una excepcion Fecha: " + DateTime.Now.ToString());
                        log.Error("Detalles:" + e.InnerException);
                        log.Error(e.Message);
                        //File.Move(file, rutaNOSubido + "\\" + Path.GetFileName(file));
                    }
                    Thread.Sleep(5000);
                }
                log.Info("SE CONCLUYO LA SUBIDA DE TODOS LOS ARCHIVOS HOMOLOGADOS. Fecha: " + DateTime.Now.ToString());
                Driver.Close();
                Driver.Dispose();
            }
            else
            {
                Driver.Close();
                Driver.Dispose();
                log.Error("No se pudo encontrar archivos csv en la carpeta "+rutaHomologacion+" . Fecha: " + DateTime.Now.ToString());                
            }
        }            
    }
}
