﻿using botDrive.Entities;
using botDrive.Helpers;
using log4net;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;
using System.Text;
using System.Linq;
using Microsoft.VisualBasic.FileIO;
using SearchOption = System.IO.SearchOption;

namespace botDrive.Proceso
{
    class HomologacionExcel
    {
        private static ILog log = LogManager.GetLogger(typeof(HomologacionExcel));
        
        private static string rutaArchivos = ConfigurationManager.AppSettings["rutaArchivosExcel"];
        private static string rutaHomologacion = ConfigurationManager.AppSettings["rutaHomologacionArchivosExcelCsv"];
        private static string rutaSubido = ConfigurationManager.AppSettings["rutaSubidaArchivosExcelCsv"];
        private static string rutaNOSubido = ConfigurationManager.AppSettings["rutaNOSubidaArchivosExcelCsv"];

        public void clasificarExcelPorEntidad()
        {
            log.Info("Inicio homologacion de archivos xlsx por entidad. Fecha: " + DateTime.Now.ToString());

            string[] files = Directory.GetFiles(rutaArchivos, "*.csv", SearchOption.TopDirectoryOnly);
            foreach (var file in files)
            {
                var datosExcel = LecturaDatos(file);
                if (datosExcel.codigo == 1)
                {
                    if (datosExcel.totalRows > 0)
                    {
                        var datosAgrupadosEntidad = new List<DataExcelAgrupadosPorEntidadRowResponse>();

                        foreach (var item in datosExcel.rows.Select(x => x.CODCARTERA).Distinct().ToList())
                        {
                            if (!String.IsNullOrEmpty(item))
                            {
                                var datoEntidad = new DataExcelAgrupadosPorEntidadRowResponse()
                                {
                                    entidad = item,
                                    datos = new List<DataExcelAgrupadosPorEntidadDetalleRowResponse>()
                                };
                                datoEntidad.datos = datosExcel.rows.Where(x => x.CODCARTERA == item).Select(x => new DataExcelAgrupadosPorEntidadDetalleRowResponse
                                {
                                    CODCARTERA = x.CODCARTERA,
                                    CEDULA = x.CEDULA,
                                    NUMEROTELEFONO = x.NUMEROTELEFONO,
                                    MENSAJE = x.MENSAJE,
                                    ASESOR = x.ASESOR,
                                    FECHAGESTION = x.FECHAGESTION,
                                    CANAL = x.CANAL,
                                    ESTADOCLIENTE = x.ESTADOCLIENTE,
                                    ESTADOCONTACTO = x.ESTADOCONTACTO,
                                    NIVEL1 = x.NIVEL1,
                                    NIVEL2 = x.NIVEL2,
                                    NIVEL3 = x.NIVEL3,
                                    NIVEL4 = x.NIVEL4,
                                    NIVEL5 = x.NIVEL5,
                                    NIVEL6 = x.NIVEL6,
                                    NIVEL7 = x.NIVEL7,
                                    NIVEL8 = x.NIVEL8,
                                    NIVEL9 = x.NIVEL9,
                                    NIVEL10 = x.NIVEL10,
                                }).ToList();


                                datosAgrupadosEntidad.Add(datoEntidad);
                            }                            
                        }

                        foreach (var rowEntidad in datosAgrupadosEntidad)
                        {
                            string entidadName = ConvertGestionEntidad.obtenerGestiones(rowEntidad.entidad);
                            if (entidadName != null && entidadName != "") { 
                                try
                                {   
                                    log.Info("Creando archivo excel para la entidad." + entidadName + " Fecha: " + DateTime.Now.ToString());
                                    Console.WriteLine("Creando archivo excel para la entidad." + entidadName + " Fecha: " + DateTime.Now.ToString());
                                    //if (File.Exists(rutaHomologacion + "\\" + entidadName + ".csv"))
                                    //{
                                    //    File.Delete(rutaHomologacion + "\\" + entidadName + ".csv");
                                    //}
                                    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();

                                    if (xlApp == null)
                                    {
                                        log.Info("Excel is not properly installed!!. Fecha: " + DateTime.Now.ToString());
                                        return;
                                    }

                                    Excel.Workbook xlWorkBook;
                                    Excel.Worksheet xlWorkSheet;
                                    object misValue = System.Reflection.Missing.Value;

                                    xlWorkBook = xlApp.Workbooks.Add(misValue);
                                    xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                                    xlWorkSheet.Cells[1, 1] = "CEDULA" + ";"
                                    + "NUMERO TELEFONO" + ";"
                                    + "MENSAJE" + ";"
                                    + "ASESOR" + ";"
                                    + "FECHA GESTION" + ";"
                                    + "CANAL" + ";"
                                    + "ESTADO CLIENTE" + ";"
                                    + "ESTADO CONTACTO" + ";"
                                    + "NIVEL 1" + ";"
                                    + "NIVEL 2" + ";"
                                    + "NIVEL 3" + ";"
                                    + "NIVEL 4" + ";"
                                    + "NIVEL 5" + ";"
                                    + "NIVEL 6" + ";"
                                    + "NIVEL 7" + ";"
                                    + "NIVEL 8" + ";"
                                    + "NIVEL 9" + ";"
                                    + "NIVEL 10";
                                    
                                    if (File.Exists(rutaHomologacion + "\\" + rowEntidad.entidad + ".csv"))
                                    {
                                        int contadoCell = xlWorkSheet.Rows.Count+1;
                                        foreach (var row in rowEntidad.datos)
                                        {
                                            xlWorkSheet.Cells[contadoCell, 1] = //row.CODCARTERA + ";" +
                                             row.CEDULA + ";" +
                                             row.NUMEROTELEFONO + ";" +
                                             row.MENSAJE + ";" +
                                             row.ASESOR + ";" +
                                             row.FECHAGESTION + ";" +
                                             row.CANAL + ";" +
                                             row.ESTADOCLIENTE + ";" +
                                             row.ESTADOCONTACTO + ";" +
                                             row.NIVEL1 + ";" +
                                             row.NIVEL2 + ";" +
                                             row.NIVEL3 + ";" +
                                             row.NIVEL4 + ";" +
                                             row.NIVEL5 + ";" +
                                             row.NIVEL6 + ";" +
                                             row.NIVEL7 + ";" +
                                             row.NIVEL8 + ";" +
                                             row.NIVEL9 + ";" +
                                             row.NIVEL10;
                                            contadoCell++;

                                            Console.WriteLine("Se añadió la linea : " +
                                                row.CEDULA + ";" +
                                                row.NUMEROTELEFONO + ";" +
                                                row.MENSAJE + ";" +
                                                row.ASESOR + ";" +
                                                row.FECHAGESTION + ";" +
                                                row.CANAL + ";" +
                                                row.ESTADOCLIENTE + ";" +
                                                row.ESTADOCONTACTO + ";" +
                                                row.NIVEL1 + ";" +
                                                row.NIVEL2 + ";" +
                                                row.NIVEL3 + ";" +
                                                row.NIVEL4 + ";" +
                                                row.NIVEL5 + ";" +
                                                row.NIVEL6 + ";" +
                                                row.NIVEL7 + ";" +
                                                row.NIVEL8 + ";" +
                                                row.NIVEL9 + ";" +
                                                row.NIVEL10 + " Al archivo de " + entidadName + ". Fecha: " + DateTime.Now.ToString()); 
                                            log.Info("Se añadió la linea : " +
                                                 row.CEDULA + ";" +
                                                 row.NUMEROTELEFONO + ";" +
                                                 row.MENSAJE + ";" +
                                                 row.ASESOR + ";" +
                                                 row.FECHAGESTION + ";" +
                                                 row.CANAL + ";" +
                                                 row.ESTADOCLIENTE + ";" +
                                                 row.ESTADOCONTACTO + ";" +
                                                 row.NIVEL1 + ";" +
                                                 row.NIVEL2 + ";" +
                                                 row.NIVEL3 + ";" +
                                                 row.NIVEL4 + ";" +
                                                 row.NIVEL5 + ";" +
                                                 row.NIVEL6 + ";" +
                                                 row.NIVEL7 + ";" +
                                                 row.NIVEL8 + ";" +
                                                 row.NIVEL9 + ";" +
                                                 row.NIVEL10 + " Al archivo de " + entidadName + ". Fecha: " + DateTime.Now.ToString());
                                        }
                                    }
                                    else
                                    {
                                        int contadoCell = 2;
                                        foreach (var row in rowEntidad.datos)
                                        {
                                            xlWorkSheet.Cells[contadoCell, 1] = //xlWorkSheet.Cells[contadoCell, 1] +//row.CODCARTERA + ";" +
                                             row.CEDULA + ";" +
                                             row.NUMEROTELEFONO + ";" +
                                             row.MENSAJE + ";" +
                                             row.ASESOR + ";" +
                                             row.FECHAGESTION + ";" +
                                             row.CANAL + ";" +
                                             row.ESTADOCLIENTE + ";" +
                                             row.ESTADOCONTACTO + ";" +
                                             row.NIVEL1 + ";" +
                                             row.NIVEL2 + ";" +
                                             row.NIVEL3 + ";" +
                                             row.NIVEL4 + ";" +
                                             row.NIVEL5 + ";" +
                                             row.NIVEL6 + ";" +
                                             row.NIVEL7 + ";" +
                                             row.NIVEL8 + ";" +
                                             row.NIVEL9 + ";" +
                                             row.NIVEL10;
                                            contadoCell++;

                                            Console.WriteLine("Se añadió la linea : " + 
                                                row.CEDULA + ";" +
                                                row.NUMEROTELEFONO + ";" +
                                                row.MENSAJE + ";" +
                                                row.ASESOR + ";" +
                                                row.FECHAGESTION + ";" +
                                                row.CANAL + ";" +
                                                row.ESTADOCLIENTE + ";" +
                                                row.ESTADOCONTACTO + ";" +
                                                row.NIVEL1 + ";" +
                                                row.NIVEL2 + ";" +
                                                row.NIVEL3 + ";" +
                                                row.NIVEL4 + ";" +
                                                row.NIVEL5 + ";" +
                                                row.NIVEL6 + ";" +
                                                row.NIVEL7 + ";" +
                                                row.NIVEL8 + ";" +
                                                row.NIVEL9 + ";" +
                                                row.NIVEL10 + " Al archivo de " + entidadName + ". Fecha: " + DateTime.Now.ToString());
                                            log.Info("Se añadió la linea : " +
                                                row.CEDULA + ";" +
                                                row.NUMEROTELEFONO + ";" +
                                                row.MENSAJE + ";" +
                                                row.ASESOR + ";" +
                                                row.FECHAGESTION + ";" +
                                                row.CANAL + ";" +
                                                row.ESTADOCLIENTE + ";" +
                                                row.ESTADOCONTACTO + ";" +
                                                row.NIVEL1 + ";" +
                                                row.NIVEL2 + ";" +
                                                row.NIVEL3 + ";" +
                                                row.NIVEL4 + ";" +
                                                row.NIVEL5 + ";" +
                                                row.NIVEL6 + ";" +
                                                row.NIVEL7 + ";" +
                                                row.NIVEL8 + ";" +
                                                row.NIVEL9 + ";" +
                                                row.NIVEL10 + " Al archivo de " + entidadName + ". Fecha: " + DateTime.Now.ToString());

                                        }
                                    }                                   
                                    if (File.Exists(rutaHomologacion + "\\" + rowEntidad.entidad + ".csv"))
                                    {
                                        xlWorkBook.Save();
                                        Console.WriteLine("Se guardó el archivo de " + entidadName + ". Fecha: " + DateTime.Now.ToString());

                                    }
                                    else
                                    {
                                        xlWorkBook.SaveAs(rutaHomologacion + "\\" + rowEntidad.entidad, Excel.XlFileFormat.xlCSV, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                                        Console.WriteLine("Se guardó el archivo de " + entidadName + ". Fecha: " + DateTime.Now.ToString());
                                    }
                                    xlWorkBook.Close(true, misValue, misValue);
                                    xlApp.Quit();

                                    Marshal.ReleaseComObject(xlWorkSheet);
                                    Marshal.ReleaseComObject(xlWorkBook);
                                    Marshal.ReleaseComObject(xlApp);

                                    log.Info("El archivo excel para la entidad fue creado. Fecha: " + DateTime.Now.ToString());

                                    try
                                    {
                                        File.Move(file, rutaSubido + "\\" + Path.GetFileName(file));
                                    }
                                    catch (Exception e)
                                    {
                                        log.Error("Ocurrio un error al mover un archivo csv para la entidad: " + entidadName + " Detalle: " + e.Message);
                                        Console.WriteLine("Ocurrio un error al mover un archivo csv para la entidad: " + entidadName + " Detalle: " + e.Message);
                                        //File.Move(file, rutaNOSubido + "\\" + Path.GetFileName(file));
                                    }
                                }
                                catch (Exception e)
                                {
                                    log.Error("Ocurrio un error al crear un archivo csv para la entidad: "+ entidadName+ " Detalle: "+e.Message);
                                    Console.WriteLine("Ocurrio un error al crear un archivo csv para la entidad: " + entidadName + " Detalle: " + e.Message);
                                    try
                                    {
                                        File.Move(file, rutaNOSubido + "\\" + Path.GetFileName(file));
                                    }
                                    catch (Exception ex)
                                    {
                                        log.Error("Ocurrio un error al mover un archivo csv para la entidad: " + entidadName + " Detalle: " + ex.Message);
                                        Console.WriteLine("Ocurrio un error al mover un archivo csv para la entidad: " + entidadName + " Detalle: " + ex.Message);
                                        //File.Move(file, rutaNOSubido + "\\" + Path.GetFileName(file));
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        log.Error("El excel no contiene suficientes datos para registrar.");
                        Console.WriteLine("El excel no contiene suficientes datos para registrar.");
                        File.Move(file, rutaNOSubido + "\\" + Path.GetFileName(file));
                    }
                }
                else
                {
                    log.Error("No se pudo leer el excel: Mensaje: " + datosExcel.mensaje + "Fecha: " + DateTime.Now.ToString());
                    Console.WriteLine("No se pudo leer el excel: Mensaje: " + datosExcel.mensaje);
                    File.Move(file, rutaNOSubido + "\\" + Path.GetFileName(file));
                }
            }
            log.Info("Finalizo homologacion de archivos xlsx por entidad. Fecha: " + DateTime.Now.ToString());
        }

        public DataExcelResponse LecturaDatos(string files)
        {
            DataExcelResponse response = new DataExcelResponse()
            {
                codigo = 1,
                mensaje = "Datos obtenidos correctamente",
                rows = new List<DataExcelRowResponse>()
            };
            try
            {
                log.Info("Lectura de csv. Fecha: " + DateTime.Now.ToString());

                var path = @files;
                using (TextFieldParser csvParser = new TextFieldParser(path))
                {
                    csvParser.CommentTokens = new string[] { "#" };
                    csvParser.SetDelimiters(new string[] { "," });
                    csvParser.HasFieldsEnclosedInQuotes = true;

                    csvParser.ReadLine();

                    int row = 1;
                    while (!csvParser.EndOfData)
                    {
                        string[] fields = csvParser.ReadFields();
                        /*string[] fila = csvParser.ReadFields();
                        string[] fields = fila[0].Split(',');*/

                        DataExcelRowResponse dataRow = new DataExcelRowResponse();
                        dataRow.numFila = row;

                        dataRow.CODCARTERA = fields[0];
                        dataRow.CEDULA = fields[1];
                        dataRow.NUMEROTELEFONO = fields[2];
                        dataRow.MENSAJE = fields[3];
                        dataRow.ASESOR = fields[4];
                        dataRow.FECHAGESTION = fields[5];
                        dataRow.CANAL = fields[6];
                        dataRow.ESTADOCLIENTE = fields[7];
                        dataRow.ESTADOCONTACTO = fields[8];
                        dataRow.NIVEL1 = fields[9];
                        dataRow.NIVEL2 = fields[10];
                        dataRow.NIVEL3 = fields[11];
                        dataRow.NIVEL4 = fields[12];
                        dataRow.NIVEL5 = fields[13];
                        dataRow.NIVEL6 = fields[14];
                        dataRow.NIVEL7 = fields[15];
                        dataRow.NIVEL8 = fields[16];
                        dataRow.NIVEL9 = fields[17];
                        dataRow.NIVEL10 = fields[18];

                        response.rows.Add(dataRow);
                        row++;
                    }
                    response.totalRows = response.rows.Count;
                    log.Info("Recorrido de datos de excel. Cantidad registros: " + response.rows.Count + ". Fecha: " + DateTime.Now.ToString());
                }
            }
            catch (Exception ex)
            {
                response.codigo = -1;
                response.mensaje = "Error interno al leer datos del csv.";
                response.error = ex.Message;
                log.Error("Error interno al leer datos del excel. Fecha: " + DateTime.Now.ToString());
                log.Error("Error detallado: " + ex.ToString() + " Fecha: " + DateTime.Now.ToString());
            }
            return response;
        }
    }
}
