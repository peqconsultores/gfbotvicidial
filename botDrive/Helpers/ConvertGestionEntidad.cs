﻿using System;
using System.Collections.Generic;
using botDrive.Constantes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace botDrive.Helpers
{
    public static class ConvertGestionEntidad
    {
        public static String obtenerGestiones(string entidad)
        {
            string nombreGestion="";
            if (entidad == "23")
            {
                nombreGestion = ConstantesEntidad.IPANU;
            }
            //else if (entidad == "2")
            //{
            //    nombreGestion = ConstantesEntidad.COBYCO;
            //}
            else if (entidad == "34")
            {
                nombreGestion = ConstantesEntidad.AMELISSA;
            }
            else if (entidad == "22")
            {
                nombreGestion = ConstantesEntidad.MAPLE_RESPIRATORY;
            }
            else if (entidad == "33")
            {
                nombreGestion = ConstantesEntidad.RAYCO;
            }
            else if (entidad == "24")
            {
                nombreGestion = ConstantesEntidad.ESIKA_COMERCIAL;
            }
            else if (entidad == "15")
            {
                nombreGestion = ConstantesEntidad.ESIKA_PREJURIDICA;
            }
            else if (entidad == "28")
            {
                nombreGestion = ConstantesEntidad.ORIFLAME;
            }
            else if (entidad == "29")
            {
                nombreGestion = ConstantesEntidad.LEBON;
            }
            else if (entidad == "30")
            {
                nombreGestion = ConstantesEntidad.DUPREE;
            }
            //else if (entidad == "11")
            //{
            //    nombreGestion = ConstantesEntidad.MICRO_CREDITO_AVAL;
            //}
            else if (entidad == "31")
            {
                nombreGestion = ConstantesEntidad.DUPREE_PREJURIDICA;
            }
            else if (entidad == "39")
            {
                nombreGestion = ConstantesEntidad.SERFIANZA;
            }
            else if (entidad == "38")
            {
                nombreGestion = ConstantesEntidad.SERFINANZA;
            }
            else if (entidad == "50")
            {
                nombreGestion = ConstantesEntidad.FONDO_DE_EMPLEADOS;
            }
            else if (entidad == "41")
            {
                nombreGestion = ConstantesEntidad.FONDO_DE_EMPLEADOS;
            }
            else if (entidad == "42")
            {
                nombreGestion = ConstantesEntidad.HA_BICICLETAS;
            }
            //else if (entidad == "18")
            //{
            //    nombreGestion = ConstantesEntidad.CAPACITACION;
            //}
            else if (entidad == "44")
            {
                nombreGestion = ConstantesEntidad.MARKETING_PERSONAL;
            }
            //else if (entidad == "21")
            //{
            //    nombreGestion = ConstantesEntidad.CAPACITACION_1;
            //}
            else
            {
                nombreGestion = null;
            }
            return nombreGestion;
        }
    }
}
