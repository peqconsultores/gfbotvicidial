﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace botDrive.Constantes
{
    public static class ConstantesEntidad
    {
        public static string IPANU = "ipanu";
        public static string COBYCO = "cobyco";
        public static string AMELISSA = "amelissa";
        public static string MAPLE_RESPIRATORY = "maple";
        public static string RAYCO = "rayco";
        public static string ESIKA_COMERCIAL = "esika com";
        public static string ESIKA_PREJURIDICA = "esika pre";
        public static string ORIFLAME = "oriflame";
        public static string LEBON = "lebon";
        public static string DUPREE = "dupree";
        public static string MICRO_CREDITO_AVAL = "micro";
        public static string DUPREE_PREJURIDICA = "dupree pre";
        public static string SERFIANZA = "serfianza";
        public static string SERFINANZA = "serfinanza";
        public static string FONDO_DE_EMPLEADOS = "fondo";
        public static string HA_BICICLETAS = "ha";
        public static string CAPACITACION = "capacitacion";
        public static string MARKETING_PERSONAL = "marketing";
        public static string CAPACITACION_1 = "capacitacion 1";
        public static string SECREDITOS_CASTIGO = "secreditos cas";
        public static string SECREDITOS_PRE = "secreditos pre";
    }
}
