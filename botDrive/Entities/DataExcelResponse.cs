﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace botDrive.Entities
{
    class DataExcelResponse
    {
        public int codigo { get; set; }
        public string mensaje { get; set; }
        public string error { get; set; }
        public int totalRows { get; set; }
        public List<DataExcelRowResponse> rows { get; set; }
    }
    public class DataExcelRowResponse
    {
        public int numFila { get; set; }
        public string CODCARTERA { get; set; }
        public string CEDULA { get; set; }
        public string NUMEROTELEFONO { get; set; }
        public string MENSAJE { get; set; }
        public string ASESOR { get; set; }
        public string FECHAGESTION { get; set; }
        public string CANAL { get; set; }
        public string ESTADOCLIENTE { get; set; }
        public string ESTADOCONTACTO { get; set; }
        public string NIVEL1 { get; set; }
        public string NIVEL2 { get; set; }
        public string NIVEL3 { get; set; }
        public string NIVEL4 { get; set; }
        public string NIVEL5 { get; set; }
        public string NIVEL6 { get; set; }
        public string NIVEL7 { get; set; }
        public string NIVEL8 { get; set; }
        public string NIVEL9 { get; set; }
        public string NIVEL10 { get; set; }
    }

    //public class DataCsvRowResponse
    //{
    //    public string CODCARTERA { get; set; }
    //    public string CEDULA { get; set; }
    //    public string NUMEROTELEFONO { get; set; }
    //    public string MENSAJE { get; set; }
    //    public string ASESOR { get; set; }
    //    public string FECHAGESTION { get; set; }
    //    public string CANAL { get; set; }
    //    public string ESTADOCLIENTE { get; set; }
    //    public string ESTADOCONTACTO { get; set; }
    //    public string NIVEL1 { get; set; }
    //    public string NIVEL2 { get; set; }
    //    public string NIVEL3 { get; set; }
    //    public string NIVEL4 { get; set; }
    //    public string NIVEL5 { get; set; }
    //    public string NIVEL6 { get; set; }
    //    public string NIVEL7 { get; set; }
    //    public string NIVEL8 { get; set; }
    //    public string NIVEL9 { get; set; }
    //    public string NIVEL10 { get; set; }
    //}
    public class DataExcelAgrupadosPorEntidadRowResponse
    {
        public string entidad { get; set; }
        public List<DataExcelAgrupadosPorEntidadDetalleRowResponse> datos { get; set; }
    }
    public class DataExcelAgrupadosPorEntidadDetalleRowResponse
    {
        public string CODCARTERA { get; set; }
        public string CEDULA { get; set; }
        public string NUMEROTELEFONO { get; set; }
        public string MENSAJE { get; set; }
        public string ASESOR { get; set; }
        public string FECHAGESTION { get; set; }
        public string CANAL { get; set; }
        public string ESTADOCLIENTE { get; set; }
        public string ESTADOCONTACTO { get; set; }
        public string NIVEL1 { get; set; }
        public string NIVEL2 { get; set; }
        public string NIVEL3 { get; set; }
        public string NIVEL4 { get; set; }
        public string NIVEL5 { get; set; }
        public string NIVEL6 { get; set; }
        public string NIVEL7 { get; set; }
        public string NIVEL8 { get; set; }
        public string NIVEL9 { get; set; }
        public string NIVEL10 { get; set; }
    }
}
