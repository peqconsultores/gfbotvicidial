﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace botDrive.Entities
{
    public class ResponseNiveles
    {
        public int codigo { get; set; }
        public string mensaje { get; set; }
        public string valor { get; set; }
        public string valor2 { get; set; }
        public string valor3 { get; set; }
        public string valor4 { get; set; }
        public string valor5 { get; set; }
        public string valor6 { get; set; }
        public string valor7 { get; set; }
        public string valor8 { get; set; }
        public string valor9 { get; set; }
        public string valor10 { get; set; }
    }
}
