﻿using botDrive.Proceso;
using log4net;

namespace botDrive
{
    static class Program
    {
        private static ILog log = LogManager.GetLogger(typeof(Program));
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();

            HomologacionExcel homologacionExcel = new HomologacionExcel();
            homologacionExcel.clasificarExcelPorEntidad();
            
            SubidaExcel subidaExcel = new SubidaExcel();
            subidaExcel.beforeText();
        }
    }
}
